#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <mpi.h>

#define DEBUG           0

#define MASTER_PROCCESS 0
#define DATA_SIZE_TAG   0
#define DATA_TAG        1
#define USE_CHANNEL_TAG 2
#define RESULT_TAG      3
#define ACTIVE_MSJ_TAG  4
#define NOT_BUSY_TAG    5
#define ROW_RESULT_TAG  6

//Prints the usage of the program if needed
void print_usage ()
{
    printf ("Usage: $ ./mxv -m matrixSize\n");
    printf (" -m matrixSize : size of the square matrix to multiply\n");
    exit (0);
}
////////////////////////////////////////
//               Main                 //
////////////////////////////////////////
int main(int argc, char * argv []) {
    
    int   l_iRank, l_iSize;
    MPI_Init(& argc,  & argv);
    MPI_Comm_rank(MPI_COMM_WORLD,  & l_iRank);
    MPI_Comm_size(MPI_COMM_WORLD,  & l_iSize);
    
    int ierr;
    
    if (l_iRank == 0) //master proccess
    {
        ////////////////////////////////////////
        // Process the command line arguments //
        ////////////////////////////////////////
        int l_iSquareMatrixSize;
              
        if ( argc == 3 )
        {
            if (strcmp ("-m", argv [1]) == 0) { l_iSquareMatrixSize = atoi (argv [2]);} 
            else { print_usage (); }        
        } else{ print_usage (); }
        ////////////////////////////////////////
        //  Create the matrix and the vector  //
        ////////////////////////////////////////
        clock_t tic = clock();
        int l_pMatrix[l_iSquareMatrixSize*l_iSquareMatrixSize];
        int l_pVector[l_iSquareMatrixSize];
        int l_pMultiplication[l_iSquareMatrixSize];
        // fill the matrix and the vector to multiplicate
        for(int i=0; i<l_iSquareMatrixSize;i++)
        {
            for(int j=0; j<l_iSquareMatrixSize;j++)
            {
                l_pMatrix[i*l_iSquareMatrixSize+j]=i+j;
            }
            l_pVector[i]=i;
        }
        //////////////
        /////////////
        //////////////
        int l_pBusy[l_iSize]; for(int i=1; i<l_iSize; i++){ l_pBusy[i]=0; }
        int l_iRowToProccess =0;
        int l_iRowsProccessed=0;
        int l_iDeactivate    =1;
        
        //Broatcast inicial
        for(int i=1; i<l_iSize;i++)
        {
            ierr=MPI_Send(         & l_iDeactivate, 1, MPI_INT, i, ACTIVE_MSJ_TAG, MPI_COMM_WORLD);
            ierr=MPI_Send(  & l_iRowToProccess, 1, MPI_INT, i,       DATA_TAG, MPI_COMM_WORLD);
            ierr=MPI_Send(&l_pMatrix[l_iSquareMatrixSize*l_iRowToProccess], l_iSquareMatrixSize, MPI_INT, i, DATA_TAG, MPI_COMM_WORLD);
            ierr=MPI_Send(&l_pVector[0], l_iSquareMatrixSize, MPI_INT, i, DATA_TAG, MPI_COMM_WORLD);
            l_pBusy[i]=1;
            #if DEBUG
            printf("\033[1;32m" "[Send Row %d to Proccess %d]" "\x1b[0m", l_iRowToProccess,i);
            #endif
            l_iRowToProccess++;
            if(l_iRowToProccess>=l_iSquareMatrixSize){break;}
        }
          
        while(l_iRowToProccess<l_iSquareMatrixSize || l_iRowsProccessed<l_iSquareMatrixSize)
        {
            // look a slave not busy
            int allBusy=-1;for(int i=1;i<l_iSize;i++){if(l_pBusy[i]==0){allBusy=i; break;}}
            // if a slave is not busy, assign work
            if(allBusy!=-1 && l_iRowToProccess<l_iSquareMatrixSize)
            {
                ierr=MPI_Send(     & l_iDeactivate, 1, MPI_INT, allBusy, ACTIVE_MSJ_TAG, MPI_COMM_WORLD);
                ierr=MPI_Send(  & l_iRowToProccess, 1, MPI_INT, allBusy,       DATA_TAG, MPI_COMM_WORLD);
                ierr=MPI_Send(&l_pMatrix[l_iSquareMatrixSize*l_iRowToProccess], l_iSquareMatrixSize, MPI_INT, allBusy, DATA_TAG, MPI_COMM_WORLD);
                ierr=MPI_Send(&l_pVector[0], l_iSquareMatrixSize, MPI_INT, allBusy, DATA_TAG, MPI_COMM_WORLD);
                #if DEBUG 
                printf("\033[1;32m" "[Send Row %d to Proccess %d]" "\x1b[0m", l_iRowToProccess,allBusy); 
                #endif
                l_iRowToProccess++;
                l_pBusy[allBusy]=1;
            }
            
            //Wait results if all are busy or no more data are need to proccess
            else if(l_iRowsProccessed<l_iSquareMatrixSize)
            {
                int l_iNumberOfSlave;
                int l_iRow;
                ierr=MPI_Recv(&l_iNumberOfSlave, 1, MPI_INT, MPI_ANY_SOURCE, USE_CHANNEL_TAG, MPI_COMM_WORLD,MPI_STATUS_IGNORE);
                ierr=MPI_Recv(&l_iRow, 1, MPI_INT, l_iNumberOfSlave, ROW_RESULT_TAG, MPI_COMM_WORLD,MPI_STATUS_IGNORE);
                ierr=MPI_Recv(&(l_pMultiplication[l_iRow]), 1, MPI_INT, l_iNumberOfSlave, RESULT_TAG, MPI_COMM_WORLD,MPI_STATUS_IGNORE);
                l_pBusy[l_iNumberOfSlave]=0;
                l_iRowsProccessed++;
                #if DEBUG
                printf("\033[1;36m" "  [Row %d ready,  %d rows proccesed]  " "\x1b[0m",l_iRow,l_iRowsProccessed );
                #endif
            }
        }
        // Deactivate slaves for terminate program
        l_iDeactivate =0;
        for(int i=1;i<l_iSize;i++)
        {
            #if DEBUG
            printf("\033[1;31m" "  [Deactivating proccess %d]  " "\x1b[0m", i );
            #endif
            ierr=MPI_Send( & l_iDeactivate, 1, MPI_INT, i, ACTIVE_MSJ_TAG, MPI_COMM_WORLD);
        }
        clock_t toc = clock();
        ///////////////////
        // print results //
        ///////////////////
        printf("\n");
        for(int i=0; i<l_iSquareMatrixSize;i++)
        {
            printf("[");
            for(int j=0; j<l_iSquareMatrixSize;j++)
            {
                printf("%4d",l_pMatrix[i*l_iSquareMatrixSize+j]);
            }
            
            printf("]");
            
            if(i!=l_iSquareMatrixSize/2){ printf("   [%4d]   [%4d]",l_pVector[i],l_pMultiplication[i]); }
            else{ printf(" x [%4d] = [%4d]",l_pVector[i],l_pMultiplication[i]); }
            
            printf("\n");
        }
        printf("\n");
        printf("Time Elapsed for execution: %f seconds\n", (double)(toc - tic) / CLOCKS_PER_SEC);
        // print non paralel execution for debug
        #if DEBUG
        int mult[l_iSquareMatrixSize];
        for(int i=0; i<l_iSquareMatrixSize;i++)
        {  
           mult[i]=0;
           for(int j=0;j<l_iSquareMatrixSize;j++){ mult[i]+=l_pMatrix[i*l_iSquareMatrixSize+j]*l_pVector[j]; }
        }
        for(int i=0; i<l_iSquareMatrixSize;i++)
        {
            printf("[");
            for(int j=0; j<l_iSquareMatrixSize;j++)
            {
                printf("%4d",l_pMatrix[i*l_iSquareMatrixSize+j]);
            }
            
            printf("]");
            
            if(i!=l_iSquareMatrixSize/2){ printf("   [%4d]   [%4d]",l_pVector[i],mult[i]); }
            else{ printf(" x [%4d] = [%4d]",l_pVector[i],mult[i]); }
            
            printf("\n");
        }
        printf("\n");
        
        int l_iSameResultDebug=1;
        for(int i=0;i<l_iSquareMatrixSize;i++){if(mult[i]!=l_pMultiplication[i]){l_iSameResultDebug=0;}}
        if(l_iSameResultDebug){printf("\033[1;32m""    Same result\n""\x1b[0m");}
        else{printf("\033[1;31m""Error\n""\x1b[0m");}
        
        #endif
    }//end master proccess
    
    ////////////////////////////////////////
    //         Slave proccesses           //
    ////////////////////////////////////////
    else
    {
        int l_iVectorSize;   // size of vectors to use
        int l_iIsActive = 1; // variable for deactivate the proccess
        int l_iResult;       // result to send
        int l_iRowOutput;    // row of the output
        
        // get the size of the vector from args
        if ( argc == 3 ){ if (strcmp ("-m", argv [1]) == 0) { l_iVectorSize = atoi (argv [2]);} } 

        // create two vector for storing the inputs
        int l_pVectorOne[l_iVectorSize];
        int l_pVectorTwo[l_iVectorSize];

        // execute while the proccess is active
        while(l_iIsActive)
        {
            //Receive next state            
            ierr=MPI_Recv(&l_iIsActive ,             1, MPI_INT, MASTER_PROCCESS, ACTIVE_MSJ_TAG, MPI_COMM_WORLD,MPI_STATUS_IGNORE);
            
            if(l_iIsActive)
            {
            // receive row of output, vectors to multiply from master
            ierr=MPI_Recv(&l_iRowOutput,             1, MPI_INT, MASTER_PROCCESS, DATA_TAG, MPI_COMM_WORLD,MPI_STATUS_IGNORE);
            ierr=MPI_Recv(&l_pVectorOne, l_iVectorSize, MPI_INT, MASTER_PROCCESS, DATA_TAG, MPI_COMM_WORLD,MPI_STATUS_IGNORE);
            ierr=MPI_Recv(&l_pVectorTwo, l_iVectorSize, MPI_INT, MASTER_PROCCESS, DATA_TAG, MPI_COMM_WORLD,MPI_STATUS_IGNORE);
            
            //calculate the result
            l_iResult = 0;
            for(int i=0;i<l_iVectorSize;i++){ l_iResult+=l_pVectorOne[i]*l_pVectorTwo[i]; }
            
            //send results back to master
            ierr=MPI_Send(&l_iRank        , 1, MPI_INT, MASTER_PROCCESS, USE_CHANNEL_TAG, MPI_COMM_WORLD);
            ierr=MPI_Send(&l_iRowOutput, 1, MPI_INT, MASTER_PROCCESS, ROW_RESULT_TAG, MPI_COMM_WORLD);
            ierr=MPI_Send(&l_iResult   , 1, MPI_INT, MASTER_PROCCESS, RESULT_TAG, MPI_COMM_WORLD);
            
            }
        }
    }

    MPI_Finalize( );
    return 0;
}
