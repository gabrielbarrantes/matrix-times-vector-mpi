# matrix times vector MPI

A program for multiplicate a square matrix and a vector using several processes.

The program takes as input the number of processes to use and the size of the 
square matrix.  

## Instructions for use:

$ sudo apt install mpich

$ mpiexec -n < # > ./mxv -m < # >

Where -n is the number of processes to use and -m the size of the square matrix.

### Example of use:
    $ mpiexec -n 4 ./mxv -m 10

### Output:
    [   0   1   2   3   4   5   6   7   8   9]   [   0]   [ 285]
    [   1   2   3   4   5   6   7   8   9  10]   [   1]   [ 330]
    [   2   3   4   5   6   7   8   9  10  11]   [   2]   [ 375]
    [   3   4   5   6   7   8   9  10  11  12]   [   3]   [ 420]
    [   4   5   6   7   8   9  10  11  12  13]   [   4]   [ 465]
    [   5   6   7   8   9  10  11  12  13  14] x [   5] = [ 510]
    [   6   7   8   9  10  11  12  13  14  15]   [   6]   [ 555]
    [   7   8   9  10  11  12  13  14  15  16]   [   7]   [ 600]
    [   8   9  10  11  12  13  14  15  16  17]   [   8]   [ 645]
    [   9  10  11  12  13  14  15  16  17  18]   [   9]   [ 690]
    
    Time Elapsed for execution: 0.001143 seconds

